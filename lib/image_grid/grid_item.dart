import 'package:bloc_api_practice/detail_screen.dart';
import 'package:bloc_api_practice/models/image_model.dart';
import 'package:flutter/material.dart';

import 'package:flutter_blurhash/flutter_blurhash.dart';

class GridItem extends StatelessWidget {
  final ImageModel imageModel;

  const GridItem({@required this.imageModel});

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (_) {
            return DetailScreen(imageModel);
          }));
        },
        child: GridTile(
          child: Image.network(
            imageModel.url,
            fit: BoxFit.cover,
            loadingBuilder: (BuildContext ctx, Widget child, ImageChunkEvent loadingProgress) {
              if (loadingProgress == null) return child;
              return BlurHash(hash: imageModel.blurHash);
            },
          ),
        ),
      ),
    );
  }
}
