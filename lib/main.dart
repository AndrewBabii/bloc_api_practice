import 'package:bloc_api_practice/bloc/image_bloc.dart';
import 'package:bloc_api_practice/bloc/image_event.dart';
import 'package:bloc_api_practice/bloc/image_state.dart';
import 'package:bloc_api_practice/image_grid/image_grid.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: BlocProvider(
          create: (BuildContext ctx) => ImageBloc(),
          child: MyHomePage(),
        ));
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ImageBloc>(context).add(ImageEvent.imageLoadEvent);
    return BlocBuilder<ImageBloc, ImageState>(
      builder: (BuildContext ctx, ImageState state) {
        if (state is ImageEmptyState) {
          return Text('');
        }

        if (state is ImageLoadingState) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is ImageLoadedState) {
          return ImageGrid(images: state.images);
        }

        return null;
      },
    );
  }
}
