import 'package:flutter/material.dart';

class ImageModel {
  final String id;
  final String name;
  final String url;
  final String blurHash;

  const ImageModel({
    @required this.id,
    @required this.name,
    @required this.url,
    @required this.blurHash,
  });

  factory ImageModel.fromJson(Map<String, dynamic> jsonData) {
    return ImageModel(
      id: jsonData['id'],
      url: jsonData['urls']['regular'],
      name: jsonData['description'],
      blurHash: jsonData['blur_hash'],
    );
  }
}
