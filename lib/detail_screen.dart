import 'package:bloc_api_practice/models/image_model.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatelessWidget {
  final ImageModel imageModel;

  DetailScreen(this.imageModel);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        children: [
          Image.network(imageModel.url),
          Center(
            child: Text(
              imageModel.name ?? '',
              style: TextStyle(color: Colors.purple, fontSize: 25),
            ),
          ),
          Center(
            child: Text(
              imageModel.id ?? '',
              style: TextStyle(color: Colors.grey, fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}
