class ImageDTO {
  final int id;
  final String name;
  final String url;

  const ImageDTO({
    this.id,
    this.name,
    this.url,
  });
}