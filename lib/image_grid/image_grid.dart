import 'package:bloc_api_practice/image_grid/grid_item.dart';
import 'package:bloc_api_practice/models/image_model.dart';
import 'package:flutter/material.dart';

class ImageGrid extends StatelessWidget {
  final List<ImageModel> images;

  const ImageGrid({@required this.images});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 15.0,
        mainAxisSpacing: 15.0,
      ),
      shrinkWrap: true,
      itemCount: images.length,
      itemBuilder: (BuildContext ctx, int index) {
        return GridItem(
          imageModel: images[index],
        );
      },
    );
  }
}
