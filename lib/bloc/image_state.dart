import 'package:bloc_api_practice/models/image_model.dart';
import 'package:flutter/material.dart';

abstract class ImageState {}

class ImageEmptyState extends ImageState {}

class ImageLoadingState extends ImageState {}

class ImageLoadedState extends ImageState {
  final List<ImageModel> images;

  ImageLoadedState({@required this.images}) : assert(images != null);
}
