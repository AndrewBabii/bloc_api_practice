import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:bloc_api_practice/models/image_model.dart';

class HttpService {
  final String baseUrl = 'https://api.unsplash.com/';
  final String getTenPhotos = 'photos?per_page=10';
  final String unsplashApiKey = 'ab3411e4ac868c2646c0ed488dfd919ef612b04c264f3374c97fff98ed253dc9';
  final String auth = 'Authorization';
  final String clientID = 'Client-ID';

  HttpService._privateConstructor();

  static final HttpService _instance = HttpService._privateConstructor();

  static HttpService get instance => _instance;

  Future<List<ImageModel>> fetchImages() async {
    Map<String, String> _headers = {auth: '$clientID $unsplashApiKey'};
    http.Response response = await http.get('$baseUrl$getTenPhotos', headers: _headers);
    print(response.body);

    if (response.statusCode == 200) {
      if (response.body == null) {
        throw Exception('Response body is empty');
      }
      final List<dynamic> _images = json.decode(response.body);
      return _images.map((json) => ImageModel.fromJson(json)).toList();
    } else {
      throw Exception('Response status error ${response.statusCode}');
    }
  }
}
