

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc_api_practice/bloc/image_event.dart';
import 'package:bloc_api_practice/bloc/image_state.dart';
import 'package:bloc_api_practice/models/image_model.dart';
import 'package:bloc_api_practice/repository.dart';

class ImageBloc extends Bloc<ImageEvent, ImageState> {
  @override
  Stream<ImageState> mapEventToState(ImageEvent event) async*{
    switch(event){
      case ImageEvent.imageLoadEvent :
        yield ImageLoadingState();
        try{
          final List<ImageModel> _loadedImages = await Repository.instance.getImagesList();
          yield ImageLoadedState(images: _loadedImages);
        }catch(e){
          throw Exception('Image Bloc - $e');
        }
        break;
      case ImageEvent.imageRemoveEvent :
        yield ImageEmptyState();
        break;
    }
  }
  ImageBloc() : super(ImageEmptyState());
}