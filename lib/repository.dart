import 'package:bloc_api_practice/http_service.dart';
import 'package:bloc_api_practice/models/image_model.dart';

class Repository {
  Repository._privateConstructor();

  static final Repository _instance = Repository._privateConstructor();

  static Repository get instance => _instance;

  Future<List<ImageModel>> getImagesList() async {
    return await HttpService.instance.fetchImages();
  }
}
